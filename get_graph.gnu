# This gnuplot script generates .ps graphs for displacement - load factor, 
# crack area - energy, and crack area - load factor (force) relations

set terminal postscript color   # eps enhanced "Verdana" 12

#
# DISPLACEMENT - LOAD FACTOR (FORCE)
#

set output "graph_displacement_force.ps"

set xlabel "Displacement (mm)"
set ylabel "Reaction (N)"
set title "Load Displacement curve for the 3D plate with circular hole"
set format y "%2.0t{/Symbol \327}10^{%L}"
set yrange [-80e3 : 80e3]
set ytics -80e3,20e3,80e3
set grid

unset key
set key left # box

plot "displacement_force.txt" u 1:2 \
title "Displacement vs. Reaction" w lp pt 7 ps 1.5 lc rgb "blue" \
lw 5 dt 1 pointinterval 1
