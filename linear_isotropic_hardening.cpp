/** \file elasticity.cpp
 * \ingroup nonlinear_elastic_elem
 * \example elasticity.cpp

 The example shows how to solve the linear elastic problem. An example can read
 file with temperature field, then thermal stresses are included.

 What example can do:
 - take into account temperature field, i.e. calculate thermal stresses and
deformation
 - stationary and time depend field is considered
 - take into account gravitational body forces
 - take in account fluid pressure
 - can work with higher order geometry definition
 - works on distributed meshes
 - multi-grid solver where each grid level is approximation order level
 - each mesh block can have different material parameters and approximation
order

See example how code can be used \cite jordi:2017,
 \image html SquelaDamExampleByJordi.png "Example what you can do with this
code. Analysis of the arch dam of Susqueda, located in Catalonia (Spain)"
width=800px

 This is an example of application code; it does not show how elements are
implemented. Example presents how to:
 - read mesh
 - set-up problem
 - run finite elements on the problem
 - assemble matrices and vectors
 - solve the linear system of equations
 - save results


 If you like to see how to implement finite elements, material, are other parts
of the code, look here;
 - Hooke material, see \ref Hooke
 - Thermal-stress assembly, see \ref  ThermalElement
 - Body forces element, see \ref BodyFroceConstantField
 - Fluid pressure element, see \ref FluidPressure
 - The general implementation of an element for arbitrary Lagrangian-Eulerian
 aformulation for a nonlinear elastic problem is here \ref
 NonlinearElasticElement. Here we limit ourselves to Hooke equation and fix
 mesh, so the problem becomes linear. Not that elastic element is implemented
 with automatic differentiation.

*/

/* MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <boost/program_options.hpp>
// #include "../poisson/src/AuxPoissonFunctions.hpp"
// #include "../poisson/src/PoissonOperators.hpp"
// #include <ElasticityNewOperator.hpp>
#include <Hooke.hpp>
#include <PlDataAtGaussPts.hpp>
#include <LinearIsotropicHardening.hpp>
#include <CalculateStress.hpp>
#include <Plasticity_Declaration.hpp>
#include <PlasUpdateSave.hpp>

using namespace boost::numeric;
using namespace MoFEM;
using namespace std;
namespace po = boost::program_options;

static char help[] = "-my_block_config set block data\n"
                     "\n";

int main(int argc, char *argv[]) {

  //Coordinates of points for acquiring data
  std::array<double, 3> Point_for_data = {100., 0., 1.};
  std::string str                      = "test";
  int Christophe_iterator;
  Christophe_iterator = 0;

  const string default_options ="-ksp_type fgmres \n"
                                 "-pc_type lu \n"
                                 "-pc_factor_mat_solver_package mumps \n"
                                 "-ksp_monitor \n"
                                 "-snes_type newtonls \n"
                                 "-snes_atol 1e-8 \n"
                                 "-snes_rtol 1e-8 \n"
                                 "-snes_monitor \n"
                                 "-ts_monitor \n"
                                 "-ts_type beuler \n";

  string param_file = "param_file.petsc";
  if (!static_cast<bool>(ifstream(param_file))) {
    std::ofstream file(param_file.c_str(), std::ios::ate);
    if (file.is_open()) {
      file << default_options;
      file.close();
    }
  }

  // Initialize PETSCc
  MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);
 
  // Create mesh database
  moab::Core mb_instance;              // create database
  moab::Interface &moab = mb_instance; // create interface to database

    MPI_Comm moab_comm_world;
  MPI_Comm_dup(PETSC_COMM_WORLD, &moab_comm_world);
  ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
  if (pcomm == NULL)
    pcomm = new ParallelComm(&moab, moab_comm_world);

  try {
    
    
    char mesh_file_name[255];
    PetscBool flg_file;
    CHKERR DMRegister_MoFEM("DMMOFEM");
    PetscBool is_partitioned = PETSC_FALSE;

    // Get command line options
    int order = 1;                    // default approximation order

    int nb_sub_steps = 90;                    // default number of steps

     double des_int_number = 4.;

    double lambda0 = 0.1;               //default step size

    PetscBool is_adaptive = PETSC_FALSE;

    PetscBool is_cyclic = PETSC_FALSE;

    PetscBool flg_test = PETSC_FALSE; // true check if error is numerical error
    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "SimpleElasticProblem",
                             "none");

    CHKERR PetscOptionsString("-file_name", "mesh file name", "", "mesh.h5m",
                              mesh_file_name, 255, &flg_file);
    // Set approximation order
    CHKERR PetscOptionsInt("-order", "approximation order", "", order, &order,
                           PETSC_NULL);

    CHKERR PetscOptionsInt("-steps", "number of steps", "", nb_sub_steps,
                           &nb_sub_steps, PETSC_NULL);

    CHKERR PetscOptionsScalar("-stepsize", "step size", "", lambda0, &lambda0,
                              PETSC_NULL);

    CHKERR PetscOptionsScalar("-des_it", "desired number of iteration", "", des_int_number, &des_int_number,
                              PETSC_NULL);

    // Set testing (used by CTest)
    CHKERR PetscOptionsBool("-test", "if true is ctest", "", flg_test,
                            &flg_test, PETSC_NULL);

    CHKERR PetscOptionsBool("-is_partitioned", "is_partitioned?", "",
                            is_partitioned, &is_partitioned, PETSC_NULL);

    CHKERR PetscOptionsBool("-is_adapt", "trigger adaptivity?", "",
                            is_adaptive, &is_adaptive, PETSC_NULL);

    CHKERR PetscOptionsBool("-is_cycle", "trigger cyclic loading?", "",
                            is_cyclic, &is_cyclic, PETSC_NULL);
    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);


    if (flg_file != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    //create mesh database
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;

        // Create moab communicator
    // Create separate MOAB communicator, it is duplicate of PETSc communicator.
    // NOTE That this should eliminate potential communication problems between
    // MOAB and PETSC functions.
    MPI_Comm moab_comm_world;
    MPI_Comm_dup(PETSC_COMM_WORLD, &moab_comm_world);
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, moab_comm_world);

          // Read whole mesh or part of is if partitioned
    if (is_partitioned == PETSC_TRUE) {
      // This is a case of distributed mesh and algebra. In that case each
      // processor
      // keep only part of the problem.
      const char *option;
      option = "PARALLEL=READ_PART;"
               "PARALLEL_RESOLVE_SHARED_ENTS;"
               "PARTITION=PARALLEL_PARTITION;";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    } else {
      // If that case we have distributed algebra, i.e. assembly of vectors and
      // matrices is in parallel, but whole mesh is stored on all processors.
      // Solver and matrix scales well, however problem set-up of problem is
      // not fully parallel.
      const char *option;
      option = "";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    }

      // Create MoFEM database and link it to MoAB
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field   = core;

    // Print boundary conditions and material parameters
    MeshsetsManager *meshsets_mng_ptr;
    CHKERR m_field.getInterface(meshsets_mng_ptr);
    CHKERR meshsets_mng_ptr->printDisplacementSet();
    CHKERR meshsets_mng_ptr->printForceSet();
    CHKERR meshsets_mng_ptr->printMaterialsSet();

      BitRefLevel bit_level0;
    bit_level0.set(0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);
    EntityHandle root_set = moab.get_root_set();

       // **** ADD FIELDS **** //
    CHKERR m_field.add_field("U", H1, AINSWORTH_LEGENDRE_BASE, 3);

    CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "U");

    CHKERR m_field.set_field_order(0, MBVERTEX, "U", 1);
    CHKERR m_field.set_field_order(0, MBEDGE, "U", order);
    CHKERR m_field.set_field_order(0, MBTRI, "U", order);
    CHKERR m_field.set_field_order(0, MBTET, "U", order);
  

     CHKERR m_field.build_fields();

     CHKERR m_field.getInterface<FieldBlas>()->setField(0, MBVERTEX, "U");

     // **** ADD MOMENTUM FLUXES **** //
     // setup elements for loading
     CHKERR MetaNeummanForces::addNeumannBCElements(m_field, "U");

     CHKERR MetaNodalForces::addElement(m_field, "U");

     CHKERR MetaEdgeForces::addElement(m_field, "U");

     // **** ADD ELEMENTS **** //
     // Add finite element (this defines element declaration comes later)
     CHKERR m_field.add_finite_element("ELASTIC");
     CHKERR m_field.modify_finite_element_add_field_row("ELASTIC", "U");
     CHKERR m_field.modify_finite_element_add_field_col("ELASTIC", "U");
     CHKERR m_field.modify_finite_element_add_field_data("ELASTIC", "U");

     // Add entities to that element
     CHKERR m_field.add_ents_to_finite_element_by_type(0, MBTET, "ELASTIC");
     // build finite elements
     CHKERR m_field.build_finite_elements();
     // build adjacencies between elements and degrees of freedom
     CHKERR m_field.build_adjacencies(bit_level0);

     // **** BUILD DM **** //
     DM dm;
     DMType dm_name = "DM_ELASTIC";
     // Register DM problem
     CHKERR DMRegister_MoFEM(dm_name);
     CHKERR DMCreate(PETSC_COMM_WORLD, &dm);
     CHKERR DMSetType(dm, dm_name);
     // Create DM instance
     CHKERR DMMoFEMCreateMoFEM(dm, &m_field, dm_name, bit_level0);
     // Configure DM form line command options (DM itself, solvers,
     // pre-conditioners, ... )
     CHKERR DMSetFromOptions(dm);
     // Add elements to dm (only one here)
     CHKERR DMMoFEMAddElement(dm, "ELASTIC");
     if (m_field.check_finite_element("PRESSURE_FE"))
       CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
     if (m_field.check_finite_element("FORCE_FE"))
       CHKERR DMMoFEMAddElement(dm, "FORCE_FE");
     // CHKERR DMMoFEMSetIsPartitioned(dm, is_partitioned); //FIXME:
     // setup the DM
     CHKERR DMSetUp(dm);

     auto my_rule = [](int row, int col, int data) {
       return 2 * (data - 1);
     };

     CommonData commonData(m_field);
     commonData.getParameters();
     cout << commonData.pOisson << "<<<<<<<<<" << commonData.yieldStress
          << "<<<<<<<<" << commonData.yOung << endl;
     boost::shared_ptr<VolumeElementForcesAndSourcesCore> elastic_fe(
         new VolumeElementForcesAndSourcesCore(m_field));
     elastic_fe->getRuleHook = my_rule;

     boost::shared_ptr<VolumeElementForcesAndSourcesCore> feLhs(
         new VolumeElementForcesAndSourcesCore(m_field));
     feLhs->getRuleHook = my_rule;
     feLhs->getOpPtrVector().push_back(
         new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));
     feLhs->getOpPtrVector().push_back(
         new OpCalculateStressLHS(m_field, "U", commonData));

     feLhs->getOpPtrVector().push_back(new OpK(commonData));



     boost::shared_ptr<FEMethod> nullFE;
     boost::shared_ptr<VolumeElementForcesAndSourcesCore> feRhs(
         new VolumeElementForcesAndSourcesCore(m_field));
     feRhs->getRuleHook = my_rule;
     feRhs->getOpPtrVector().push_back(
         new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));
     feRhs->getOpPtrVector().push_back(
         new OpCalculateStressRHS(m_field, "U", commonData));
     feRhs->getOpPtrVector().push_back(new OpAssembleRhs(commonData));

     boost::shared_ptr<VolumeElementForcesAndSourcesCore> feUpdate(
         new VolumeElementForcesAndSourcesCore(m_field));
     feUpdate->getRuleHook = my_rule;
     feUpdate->getOpPtrVector().push_back(
         new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));

     feUpdate->getOpPtrVector().push_back(
         new OpCalculateStressRHS(m_field, "U", commonData));
     feUpdate->getOpPtrVector().push_back(
         new OpUpdate(m_field, "U", commonData));
     feUpdate->getOpPtrVector().push_back(
         new OpCalculateVectorFieldValues<3>("U", commonData.dispPtr));
      feUpdate->getOpPtrVector().push_back(
         new OpPrint(Point_for_data, str, commonData, m_field, Christophe_iterator));

     // moab_instance
     moab::Core mb_post;                   // create database
     moab::Interface &moab_proc = mb_post; // create interface to database

     feUpdate->getOpPtrVector().push_back(
         new OpSavePlasticStrain(m_field, "U", commonData, moab_proc));
    //  feUpdate->getOpPtrVector().push_back(
    //      new OpPrint(Point_for_data, str, commonData, m_field));
     Mat Aij;
     Vec d, F_ext, d0;
     Vec F_init;

     {
       CHKERR DMCreateGlobalVector_MoFEM(dm, &d);
       CHKERR VecDuplicate(d, &F_ext);
       CHKERR VecDuplicate(d, &F_init);
       CHKERR DMCreateMatrix_MoFEM(dm, &Aij);
       CHKERR VecZeroEntries(d);
       CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
       CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
       CHKERR DMoFEMMeshToLocalVector(
           dm, d, INSERT_VALUES,
           SCATTER_REVERSE); // TODO: ask, global/local?
       CHKERR MatZeroEntries(Aij);
    }

    


    boost::shared_ptr<DirichletDisplacementBc> dirichlet_bc_ptr(
        new DirichletDisplacementBc(m_field, "U", Aij, d, F_ext));
    dirichlet_bc_ptr->methodsOp.push_back(new LoadScale());


     // Assemble pressure and traction forces. Run particular implemented for do
    // this, see
    // MetaNeummanForces how this is implemented.
    boost::ptr_map<std::string, NeummanForcesSurface> neumann_forces;
    CHKERR MetaNeummanForces::setMomentumFluxOperators(
        m_field, neumann_forces, NULL, "U"); // TODO: ask F
    {
      auto fit = neumann_forces.begin();
      for (; fit != neumann_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale());
      }
    }


    // // Assemble forces applied to nodes, see implementation in NodalForce
    boost::ptr_map<std::string, NodalForce> nodal_forces;
    CHKERR MetaNodalForces::setOperators(m_field, nodal_forces, NULL, "U");
    {
      auto fit = nodal_forces.begin();
      for (; fit != nodal_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale());
      }
    }
  
    // // Assemble edge forces
    boost::ptr_map<std::string, EdgeForce> edge_forces;
    CHKERR MetaEdgeForces::setOperators(m_field, edge_forces, NULL, "U");
    {
      auto fit = edge_forces.begin();
      for (; fit != edge_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale());
      }
    }


    boost::shared_ptr<FEMethod> fe_null;

    // Rhs
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                  fe_null);
    {
      auto fit = neumann_forces.begin();
      for (; fit != neumann_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }
    {
      auto fit = nodal_forces.begin();
      for (; fit != nodal_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }
    {
      auto fit = edge_forces.begin();
      for (; fit != edge_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }

    dirichlet_bc_ptr->snes_ctx = SnesMethod::CTX_SNESNONE;
    dirichlet_bc_ptr->snes_x   = d;
    CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
    CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);

  // Rhs
    CHKERR DMMoFEMSNESSetFunction(dm, "ELASTIC", feRhs, fe_null, fe_null);
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                  dirichlet_bc_ptr);
    // Lhs
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                  fe_null);
    CHKERR DMMoFEMSNESSetJacobian(dm, "ELASTIC", feLhs, fe_null, fe_null);
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                  dirichlet_bc_ptr);

    SNESConvergedReason snes_reason;
  int number_of_diverges = 0;
  int desired_iteration_number = 5;
    SNES snes;
    SnesCtx *snes_ctx;
    VectorDouble reactions(3);
    {
      CHKERR SNESCreate(PETSC_COMM_WORLD, &snes);
      CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
      CHKERR SNESSetFunction(snes, F_ext, SnesRhs, snes_ctx);
      CHKERR SNESSetJacobian(snes, Aij, Aij, SnesMat, snes_ctx);
      CHKERR SNESSetFromOptions(snes);
    }

    CHKERR VecAssemblyBegin(d);
    CHKERR VecAssemblyEnd(d);

 

    PostProcVolumeOnRefinedMesh post_proc(m_field);
    CHKERR post_proc.generateReferenceElementMesh();
    CHKERR post_proc.addFieldValuesPostProc("U");
    CHKERR post_proc.addFieldValuesGradientPostProc("U");
    boost::ptr_vector<MoFEM::ForcesAndSourcesCore::UserDataOperator>
        &list_of_operators = post_proc.getOpPtrVector();

    list_of_operators.push_back(
        new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));

   // list_of_operators.push_back(new OpCalculateVectorFieldValues<3>("U", commonData.dispPtr, MBTET));

    LoadScale::lambda  = lambda0;
    double lambda_prev = lambda0;
    double delta_lambda;
    double lambda_prev_prev;

    // double lambda0 = 0.1;
    for (int ss = 0; ss < nb_sub_steps; ++ss) {

      CHKERR DMoFEMMeshToLocalVector(dm, d, INSERT_VALUES, SCATTER_FORWARD);
      dirichlet_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE;
      dirichlet_bc_ptr->snes_x   = d;
      CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
      CHKERR VecAssemblyBegin(d);
      CHKERR VecAssemblyEnd(d);

      //Monitor monitor(m_field, commonData);
 //     monitor.postProcess();

      CHKERR SNESSolve(snes, PETSC_NULL, d);

//      monitor.postProcess();
      CHKERR SNESGetConvergedReason(snes, &snes_reason);
      int its;

      CHKERR SNESGetIterationNumber(snes, &its);
      if(is_cyclic){
   if(ss < nb_sub_steps/2){

     LoadScale::lambda +=
         lambda_prev *
         pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
     lambda_prev *=
         pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
   }
    else{
      if(ss == nb_sub_steps/2 ){
      lambda_prev = lambda0;
      LoadScale::lambda  -= lambda0;
    }
    LoadScale::lambda -=
        lambda_prev *
        pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
    lambda_prev *=
        pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
   }
      }
      else{
        LoadScale::lambda +=
            lambda_prev *
            pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
            //delta_lambda = LoadScale::lambda - lambda_prev;
            lambda_prev_prev = lambda_prev; 
        lambda_prev *=
            pow(des_int_number / its, commonData.adaptPowCoef * is_adaptive);
            lambda_prev_prev += lambda_prev; 
            delta_lambda = LoadScale::lambda - lambda_prev_prev;
            if (lambda_prev <= 1){
              LoadScale::lambda += 0.0005; 
            }
      }

      CHKERR PetscPrintf(PETSC_COMM_WORLD,
                         "number of Newton iterations = %D\n\n", its);
      moab_proc.delete_mesh();
      CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", feUpdate);

      CHKERR VecDuplicate(d, &d0);
      CHKERR VecCopy(d, d0);
      CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);

      CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToGlobalVector(dm, d0, INSERT_VALUES, SCATTER_REVERSE);
    if (is_adaptive){
      if (snes_reason < 0) {
        CHKERR PetscPrintf(
            PETSC_COMM_WORLD,
            "Nonlinear solver diverged! code will try to adapt...\n");
        if (number_of_diverges < commonData.maxDiv) {
          CHKERR VecCopy(d0, d);
          LoadScale::lambda -= 2 * lambda0;
          LoadScale::lambda += commonData.stepRed * (lambda0);
          CHKERR PetscPrintf(PETSC_COMM_WORLD, "Reducing step... \n");
          number_of_diverges++;
          ss--;
          continue;

        } else {
          break;
        }
      }
    }

      CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", &post_proc);
      string out_file_name;
      std::ostringstream stm;
      stm << "out_" << ss << ".h5m";
      out_file_name = stm.str();
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "out file %s\n",
                         out_file_name.c_str());
      CHKERR post_proc.postProcMesh.write_file(out_file_name.c_str(), "MOAB",
                                               "PARALLEL=WRITE_PART");
      std::ostringstream ostrm;
      ostrm << "plast_out_" << ss << ".vtk";
      out_file_name = ostrm.str();
      CHKERR moab_proc.write_file(out_file_name.c_str(), "VTK", "");
      Christophe_iterator = 0;

      if (commonData.calcReactions) {
        CHKERR commonData.calcReactionForces(reactions, dm, feRhs,
                                             commonData.reactionsSidesetId);
        CHKERR PetscPrintf(
            PETSC_COMM_WORLD,
            "Step: %d | Lambda: %6.4e | Reactions xyz:  %6.4e  %6.4e  %6.4e | "
            "Reaction norm: %6.4e   \n",
            ss, LoadScale::lambda, reactions[0], reactions[1], reactions[2],
            sqrt(reactions[0] * reactions[0] + reactions[1] * reactions[1] +
                 reactions[2] * reactions[2]));

      } else {
        CHKERR PetscPrintf(PETSC_COMM_WORLD, "Step: %d | Lambda: %6.4e  \n", ss,
                           LoadScale::lambda);
      }
      // ### ATOM TESTS
      if (commonData.isAtomTest)
        CHKERR commonData.atomTests(ss, reactions);
    }  


  
    CHKERR SNESDestroy(&snes);

   

    CHKERR MatDestroy(&Aij);
    CHKERR VecDestroy(&d);
    CHKERR VecDestroy(&d0);
    CHKERR VecDestroy(&F_ext);
    CHKERR VecDestroy(&F_init);
    CHKERR DMDestroy(&dm);

    // This is a good reference for the future
  }
  CATCH_ERRORS;

  // finish work cleaning memory, getting statistics, etc
  MoFEM::Core::Finalize();

  return 0;
}

MoFEMErrorCode
CommonData::calcReactionForces(VectorDouble &reactions, DM &dm,
                   boost::shared_ptr<VolumeElementForcesAndSourcesCore> feRhs,
                   int &reactionsSidesetId)
{
  MoFEMFunctionBegin;

  Vec F_int;
  CHKERR DMCreateGlobalVector_MoFEM(dm, &F_int);

  feRhs->snes_ctx = FEMethod::CTX_SNESSETFUNCTION;
  feRhs->snes_f = F_int;
  CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", feRhs);

  CHKERR VecAssemblyBegin(F_int);
  CHKERR VecAssemblyEnd(F_int);
  CHKERR VecGhostUpdateBegin(F_int, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(F_int, INSERT_VALUES, SCATTER_FORWARD);

  Reactions my_react(mField, "DM_ELASTIC", "U");
  CHKERR my_react.calculateReactions(F_int);

  const auto &reactions_map = my_react.getReactionsMap();
  const int id = reactionsSidesetId;
  reactions.clear();
  const bool check_key = reactions_map.find(id) == reactions_map.end();
  if (!check_key) {
    reactions = reactions_map.at(id);
  }
  Vec react_vec;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &react_vec);
  for (int dir : {0, 1, 2}) {
    CHKERR VecZeroEntries(react_vec);
    const int idx = mField.get_comm_rank();
    CHKERR VecSetValues(react_vec, 1, &idx, &reactions[dir], INSERT_VALUES);
    CHKERR VecAssemblyBegin(react_vec);
    CHKERR VecAssemblyEnd(react_vec);
    double sum = 0;
    CHKERR VecSum(react_vec, &sum);
    reactions[dir] = sum;
  }
  CHKERR VecDestroy(&react_vec);
  CHKERR VecDestroy(&F_int);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode CommonData::atomTests(const int step_number,
                                     const VectorDouble &reactions) {

  MoFEMFunctionBegin;

  if (testNb == 1) {
    if (step_number == 29) {
       int norm_reac;
       norm_reac =
           sqrt(reactions[0] * reactions[0] + reactions[1] * reactions[1] +
                reactions[2] * reactions[2]);
       if (fabs(norm_reac - 3.8638e+04) > 1e-3) {
         CHKERR PetscPrintf(PETSC_COMM_WORLD, "norm error: %g \n",
                            norm_reac - 3.8638e+04);
         SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
                 "atom test diverged!");
      }
    }
  }

  MoFEMFunctionReturn(0);
}
