#!/bin/bash
# This script reads the log file then extracts result data and 
# plots relation graphs in MoFEM fracture module.
# Run this script and it will call get_graphs.gnu which is a gnuplot script


# Create raw data file of displacement - force - crack area - energy
# from log file (NOT including 'Not Converged Propagation')
grep "Reaction" log | \
awk '{print $5,$10}'| \
tee data_raw.dat

# Keep updated data only (in case of restart) and write it to a text file
# tail -r data_raw.dat | sort -u -n | tee displacement_force.txt
mv data_raw.dat displacement_force.txt

# Print out final data file
cat displacement_force.txt     

# Make graphs using gnuplot. Note: ignore the first row of zeros when plotting
# crack area
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
gnuplot -e "l '$DIR/get_graph.gnu'; q"

# Convert graphs on .ps to .pdf
for i in `ls *.ps`; do
    ps2pdf $i;
    rm $i;
    # echo 'Converted $i into pdf, deleted original file.';
done

# Remove unnecessary files
rm -f *.dat

# Open pdf graphs
machine="$(uname -s)"
# echo ${machine}
if [ ${machine} = "Darwin" ]
then
    # echo "This is a Mac computer"
    open graph_*.pdf
else
    echo "PDF graph are ready to open"
fi
