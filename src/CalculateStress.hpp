#ifndef __PLASTICRETURN_HPP__
#define __PLASTICRETURN_HPP__

struct OpCalculateStressLHS
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface &mField;
  CommonData &commonData;

  OpCalculateStressLHS(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data) {}



  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      MoFEMFunctionReturnHot(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      MoFEMFunctionReturnHot(0);
    int nb_gauss_pts = data.getN().size1();
    const int nb_base_functions = data.getN().size2();
    auto diff_base_functions    = data.getFTensor1DiffN<3>();

    auto t_grad = getFTensor2FromMat<3, 3>(*commonData.gradDispPtr);

    commonData.stressPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_stress = getFTensor2FromMat<3,3>(*commonData.stressPtr);

    commonData.plasticStrainPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_plastic_str = getFTensor2FromMat<3,3>(*commonData.plasticStrainPtr);

    commonData.backStressPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_back_stress = getFTensor2FromMat<3, 3>(*commonData.backStressPtr);

    commonData.hardParamPtr.get()->resize(nb_gauss_pts);
    auto hp = getFTensor0FromVec(*commonData.hardParamPtr);

    commonData.kinParamPtr.get()->resize(nb_gauss_pts);
    auto kp   = getFTensor0FromVec(*commonData.kinParamPtr);

    commonData.kinParamPrevPtr.get()->resize(nb_gauss_pts);
    auto kp_prev = getFTensor0FromVec(*commonData.kinParamPrevPtr);

    MatrixDouble &Mat_Tangent = commonData.consistentMatrix;
    Mat_Tangent.resize(81, nb_gauss_pts, false);
    Mat_Tangent.clear();
    FTensor::Tensor4<double *, 3, 3, 3, 3> MT_1(TENSOR4_MAT_PTR(Mat_Tangent));

    EntityHandle fe_ent = getFEEntityHandle();
    CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.plasticStrainPtr,
                              commonData.thPlasticStrain);

    CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.hardParamPtr,
                              commonData.thHardParam);

    CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.backStressPtr,
                              commonData.thBackStress);

    CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.kinParamPtr,
                              commonData.thKinParam);

    for (int gg = 0; gg != nb_gauss_pts; gg++) {

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      FTensor::Index<'l', 3> l;

      // calculate the total t_strain tensor

      FTensor::Tensor2<double, 3, 3> t_strain; ///< t_strain tensor \f$
                                               ///< \boldsymbol{\varepsilon} =
                                               ///< \frac{1}{2}(\Delta u +
                                               ///< \Delta^{T} u) \f$
      t_strain(i, j) = 0.5 * (t_grad(i, j) + t_grad(j, i));

      // calculate the elastic t_strain tensor

      FTensor::Tensor2<double, 3, 3>
          t_elastic_strain; ///< elastic t_strain tensor \f$
                            ///< \boldsymbol{\varepsilon}^{e} =
                            ///< \boldsymbol{\varepsilon} -
                            ///< \boldsymbol{\varepsilon}^{p} \f$
      t_elastic_strain(i, j) = t_strain(i, j) - t_plastic_str(i, j);

      // calculate the trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_trial_stress; ///< trial t_stress tensor \f$
                          ///< \boldsymbol{\sigma}^{\rm{trial}} = \mathbb{D} :
                          ///< \boldsymbol{\varepsilon}^{e}\f$
      t_trial_stress(i, j) = commonData.tD(i, j, k, l) * t_elastic_strain(k, l);

      double trace_elastic_strain_3;
      trace_elastic_strain_3 =
          (1. / 3.) * (t_elastic_strain(0, 0) + t_elastic_strain(1, 1) +
                       t_elastic_strain(2, 2));
      // calculate the volumetric part of the t_strain tensor

      FTensor::Tensor2<double, 3, 3>
          t_vol_strain; ///< volumetric part of the trial elastic t_strain \f$
                        ///< \boldsymbol{\varepsilon}_{\rm{vol}}^{e} =
                        ///< \frac{1}{3}(\mathbf{I} \otimes \mathbf{I}) :
                        ///< \boldsymbol{\varepsilon}^{e} \f$

      //   t_vol_strain(i, j) =
      //       commonData.tVolProjector(i, j, k, l) * t_elastic_strain(k, l);

      t_vol_strain(i, j) = 0.;
      t_vol_strain(0, 0) = trace_elastic_strain_3;
      t_vol_strain(1, 1) = trace_elastic_strain_3;
      t_vol_strain(2, 2) = trace_elastic_strain_3;

      // calculate the hydraulic pressure

      FTensor::Tensor2<double, 3, 3>
          t_vol_pressure; ///< volumetric part of the trial t_stress \f$
                          ///< \boldsymbol{\sigma}_{\rm{vol}}^{\rm{trial}} = 3
                          ///< \kappa \boldsymbol{\varepsilon}_{\rm{vol}}^{e}
                          ///< \f$

      t_vol_pressure(i, j) = 3. * commonData.kAppa * t_vol_strain(i, j);

      // calculate the deviatoric part of the elastic t_strain tensor

      FTensor::Tensor2<double, 3, 3>
          t_dev_elastic_strain; ///< deviatoric part of the elastic trial
                                ///< t_strain \f$
                                ///< \boldsymbol{\varepsilon}^{e}_{\rm{dev}} =
                                ///< (\mathbb{I}^{S} -
                                ///< \frac{1}{3}\mathbf{I}\otimes\mathbf{I}) :
                                ///< \boldsymbol{\varepsilon}^{e}\f$

      t_dev_elastic_strain(i, j) = t_elastic_strain(i, j);
      t_dev_elastic_strain(0, 0) -= trace_elastic_strain_3;
      t_dev_elastic_strain(1, 1) -= trace_elastic_strain_3;
      t_dev_elastic_strain(2, 2) -= trace_elastic_strain_3;

      // calculate the deviatoric part of the trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_dev_trial_stress; ///<  deviatoric part of the trial t_stress \f$
                              ///<  \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}}
                              ///<  = \left(\frac{1}{2}(\mathbb{I} +
                              ///<  \mathbb{I}_{T})-\frac{1}{3}\mathbf{I}
                              ///<  \otimes \mathbf{I} \right) :
                              ///<  \boldsymbol{\sigma}^{\rm{trial}} = 2 \mu
                              ///<  \boldsymbol{\varepsilon}_{\rm{dev}}^{e}  \f$

      t_dev_trial_stress(i, j) =
          2. * commonData.Mu * t_dev_elastic_strain(i, j);

      // calculate the reduced deviatoric trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_red_dev_stress; ///< evaluate the reduced t_stress for kinematic
                            ///< hardening \f$ \boldsymbol{\eta}_{\rm{trial}} =
                            ///< \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}} -
                            ///< \boldsymbol{\beta}  \f$

      t_red_dev_stress(i, j) = t_dev_trial_stress(i, j) - t_back_stress(i, j);

      // compute the Von Mises Criterion

      double sum_entries; ///< Compute the square of the Von Mises criterion for
                          ///< trial \f$ \left(q^{\rm{trial}}\right)^{2} =
                          ///< \frac{3}{2}||\boldsymbol{\eta}^{\rm{trial}}||^{2}
                          ///< \f$

      sum_entries = (3. / 2.) * t_red_dev_stress(i, j) * t_red_dev_stress(i, j);

      // calculate the norm of the deviatoric t_stress tensor

      double norm_red_dev_stress; ///< Compute the norm of the deviatoric
                                  ///< reduced t_stress tensor \f$
                                  ///< ||\boldsymbol{\eta}^{\rm{trial}|| \f$

      norm_red_dev_stress =
          sqrt(t_red_dev_stress(i, j) * t_red_dev_stress(i, j));

      // calculate the yield function
      commonData.yieldCondition = sqrt(sum_entries) - (commonData.yieldStress +
                                                       commonData.hardMod * hp);

      kp_prev = kp;

      if (commonData.yieldCondition > 0) { // If the yield condition is
                                           // positive, return to the yield
                                           // surface
        double g_Value; ///< \f$\frac{\rm{d}\Phi}{\rm{d}\Delta\gamma}\f$
        commonData.cOunter = 0;

        // calculate the flow vector
        FTensor::Tensor2<double, 3, 3>
            t_flow_vector; ///< Calculate the flow vector \f$ \mathbf{N} =
                           ///< \frac{\boldsymbol{\eta}}{||\boldsymbol{\eta}||}
                           ///< \f$
        t_flow_vector(i, j) = t_red_dev_stress(i, j) / norm_red_dev_stress;

        // set plastic multiplier to 0 each time
        commonData.plasticMultiplier = 0.0;

        // Newton-Raphson loop to calculate the plastic multiplier
        do {
          g_Value =
              -3. * commonData.Mu - commonData.hardMod - commonData.kinMod;

          // update the plastic multiplier
          commonData.plasticMultiplier += -commonData.yieldCondition / g_Value;

          commonData.cOunter++;

        } while (fabs(sqrt(sum_entries) -
                      3. * commonData.Mu * commonData.plasticMultiplier -
                      commonData.yieldStress -
                      commonData.kinMod * commonData.plasticMultiplier -
                      (hp + commonData.plasticMultiplier) *
                          commonData.hardMod) > commonData.tOlerance &&
                 commonData.cOunter != 2000);

        // update the hardening parameter
        hp += commonData.plasticMultiplier;

        // Update kinematic hardening parameter
        kp = commonData.kinMod * hp;

        // Update the back t_stress
        t_back_stress(i, j) +=
            sqrt(2. / 3.) * (kp - kp_prev) * t_flow_vector(i, j);

        // Update the plastic strains
        t_plastic_str(i, j) +=
            commonData.plasticMultiplier * sqrt(3. / 2.) * t_flow_vector(i, j);

        // Update the deviatoric part of the t_stress tensor
        FTensor::Tensor2<double, 3, 3>
            t_dev_stress; ///< Update the deviatoric t_stress \f$
                          ///< \boldsymbol{\sigma}_{\rm{dev}} =
                          ///< \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}} - 2
                          ///< \mu \Delta \gamma \sqrt{\frac{3}{2}} \mathbf{N}
                          ///< \f$

        t_dev_stress(i, j) = t_dev_trial_stress(i, j) -
                             2. * commonData.Mu * commonData.plasticMultiplier *
                                 sqrt(3. / 2.) * t_flow_vector(i, j);

        // Update the elastic t_strain tensor
        t_elastic_strain(i, j) =
            (1. / (2. * commonData.Mu)) * t_dev_stress(i, j) +
            t_vol_strain(i, j);

        // Update the total t_stress
        t_stress(i, j) = t_dev_stress(i, j) + t_vol_pressure(i, j);

        // calculate the cross product of the flow tensor by itself
        FTensor::Tensor4<double, 3, 3, 3, 3>
            t_flow_tensor; ///< Calculate the flow
                           ///< tensor \f$
                           ///< \mathbf{N} \otimes
                           ///< \mathbf{N} \f$
        t_flow_tensor(i, j, k, l) = t_flow_vector(i, j) * t_flow_vector(k, l);

        // scale the deviatoric projector of the consistent stiffness matrix
        double numerical_scalar_1;
        numerical_scalar_1 =
            2. * commonData.Mu *
            (1. - ((commonData.plasticMultiplier * 3. * commonData.Mu) /
                   sqrt(sum_entries)));

        // numerical parameter for tensor flow in consistent stiffness matrix
        double numerical_scalar_2;
        numerical_scalar_2 =
            6. * commonData.Mu * commonData.Mu *
            ((commonData.plasticMultiplier) / sqrt(sum_entries) -
             (1.) /
                 (3. * commonData.Mu + commonData.hardMod + commonData.kinMod));

        MT_1(i, j, k, l) = 0;
        MT_1(0, 0, 0, 0) = (2. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(1, 1, 1, 1) = (2. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(2, 2, 2, 2) = (2. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(0, 0, 1, 1) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(0, 0, 2, 2) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(1, 1, 2, 2) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(2, 2, 1, 1) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(1, 1, 2, 2) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(2, 2, 0, 0) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(1, 1, 0, 0) = -(1. / 3.) * numerical_scalar_1 + commonData.kAppa;
        MT_1(0, 1, 0, 1) = 0.5 * numerical_scalar_1;
        MT_1(0, 2, 0, 2) = 0.5 * numerical_scalar_1;
        MT_1(1, 2, 1, 2) = 0.5 * numerical_scalar_1;
        MT_1(0, 1, 1, 0) = 0.5 * numerical_scalar_1;
        MT_1(0, 2, 2, 0) = 0.5 * numerical_scalar_1;
        MT_1(1, 2, 2, 1) = 0.5 * numerical_scalar_1;
        MT_1(2, 0, 0, 2) = 0.5 * numerical_scalar_1;
        MT_1(1, 0, 0, 1) = 0.5 * numerical_scalar_1;
        MT_1(2, 1, 1, 2) = 0.5 * numerical_scalar_1;
        MT_1(2, 0, 2, 0) = 0.5 * numerical_scalar_1;
        MT_1(1, 0, 1, 0) = 0.5 * numerical_scalar_1;
        MT_1(2, 1, 2, 1) = 0.5 * numerical_scalar_1;

        for (int ii = 0; ii != 3; ++ii) {
          for (int jj = 0; jj != 3; ++jj) {
            for (int kk = 0; kk != 3; ++kk) {
              for (int ll = 0; ll != 3; ++ll) {

                // Consistent stiffness matrix (at Gauss point)
                MT_1(ii, jj, kk, ll) +=
                    numerical_scalar_2 * t_flow_tensor(ii, jj, kk, ll);
              }
            }
          }
        }

      }

      else { // If the yield function is negative calculate the t_stress as if
             // it is an elastic step

        // Calculate the elastoplastic consistent tangent matrix

        FTensor::Index<'i', 3> i;
        FTensor::Index<'j', 3> j;
        FTensor::Index<'k', 3> k;
        FTensor::Index<'l', 3> l;

        MT_1(i, j, k, l) = commonData.tD(i, j, k, l);
        t_stress(i, j)   = commonData.tD(i, j, k, l) * t_elastic_strain(k, l);
      }
      ++MT_1;
      ++t_stress;
      ++t_grad;
      ++hp;
      ++t_back_stress;
      ++kp;
      ++kp_prev;
      ++t_plastic_str;
  }

  MoFEMFunctionReturn(0);
  }
};

struct OpCalculateStressRHS
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface &mField;
  CommonData &commonData;

  OpCalculateStressRHS(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data) {}



  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      MoFEMFunctionReturnHot(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      MoFEMFunctionReturnHot(0);
    int nb_gauss_pts = data.getN().size1();
    const int nb_base_functions = data.getN().size2();
    auto diff_base_functions    = data.getFTensor1DiffN<3>();

    auto t_grad = getFTensor2FromMat<3, 3>(*commonData.gradDispPtr);

    commonData.stressPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_stress = getFTensor2FromMat<3,3>(*commonData.stressPtr);

    commonData.plasticStrainPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_plastic_str = getFTensor2FromMat<3, 3>(*commonData.plasticStrainPtr);

    commonData.backStressPtr.get()->resize(9, nb_gauss_pts, false);
    auto t_back_stress = getFTensor2FromMat<3, 3>(*commonData.backStressPtr);

    commonData.hardParamPtr.get()->resize(nb_gauss_pts);
    auto hp = getFTensor0FromVec(*commonData.hardParamPtr);

    commonData.kinParamPtr.get()->resize(nb_gauss_pts);
    auto kp   = getFTensor0FromVec(*commonData.kinParamPtr);

    commonData.kinParamPrevPtr.get()->resize(nb_gauss_pts);
    auto kp_prev = getFTensor0FromVec(*commonData.kinParamPrevPtr);

    EntityHandle fe_ent = getFEEntityHandle();
    CHKERR commonData.getTags(fe_ent, nb_gauss_pts,
                                  commonData.plasticStrainPtr,
                                  commonData.thPlasticStrain);

     CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.hardParamPtr,
                                  commonData.thHardParam);
    CHKERR  commonData.getTags(
      fe_ent, nb_gauss_pts, commonData.backStressPtr, commonData.thBackStress);

     CHKERR commonData.getTags(fe_ent, nb_gauss_pts, commonData.kinParamPtr,
                                  commonData.thKinParam);

    for (int gg = 0; gg != nb_gauss_pts; gg++) {

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      FTensor::Index<'l', 3> l;

      // calculate the total t_strain tensor

      FTensor::Tensor2<double, 3, 3>
          t_strain; ///< t_strain tensor \f$
                  ///< \boldsymbol{\varepsilon} =
                  ///< \frac{1}{2}(\Delta u + \Delta^{T}
                  ///< u) \f$

      t_strain(i, j) = 0.5 * (t_grad(i, j) + t_grad(j, i));


      // calculate the elastic t_strain tensor

      FTensor::Tensor2<double, 3, 3>
          t_elastic_strain; ///< elastic t_strain tensor \f$
                          ///< \boldsymbol{\varepsilon}^{e} =
                          ///< \boldsymbol{\varepsilon} -
                          ///< \boldsymbol{\varepsilon}^{p} \f$

      t_elastic_strain(i, j) = t_strain(i, j) - t_plastic_str(i, j);


      // calculate the trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_trial_stress; ///< trial t_stress tensor \f$
                        ///< \boldsymbol{\sigma}^{\rm{trial}} = \mathbb{D} :
                        ///< \boldsymbol{\varepsilon}^{e}\f$

      t_trial_stress(i, j) =
          commonData.tD(i, j, k, l) * t_elastic_strain(k, l);

double trace_elastic_strain_3;
  trace_elastic_strain_3 =
      (1. / 3.) *
      (t_elastic_strain(0, 0) + t_elastic_strain(1, 1) + t_elastic_strain(2, 2));
  // calculate the volumetric part of the t_strain tensor

  FTensor::Tensor2<double, 3, 3>
      t_vol_strain; ///< volumetric part of the trial elastic t_strain \f$
                   ///< \boldsymbol{\varepsilon}_{\rm{vol}}^{e} =
                   ///< \frac{1}{3}(\mathbf{I} \otimes \mathbf{I}) :
                   ///< \boldsymbol{\varepsilon}^{e} \f$

//   t_vol_strain(i, j) =
//       commonData.tVolProjector(i, j, k, l) * t_elastic_strain(k, l);

  t_vol_strain(i, j) = 0.;
  t_vol_strain(0,0) = trace_elastic_strain_3;
  t_vol_strain(1,1) = trace_elastic_strain_3;
  t_vol_strain(2,2) = trace_elastic_strain_3;


  // calculate the hydraulic pressure

  FTensor::Tensor2<double, 3, 3>
      t_vol_pressure; ///< volumetric part of the trial t_stress \f$
                     ///< \boldsymbol{\sigma}_{\rm{vol}}^{\rm{trial}} = 3 \kappa
                     ///< \boldsymbol{\varepsilon}_{\rm{vol}}^{e} \f$

  t_vol_pressure(i, j) = 3. * commonData.kAppa * t_vol_strain(i, j);


//calculate the deviatoric part of the elastic t_strain tensor

 
 FTensor::Tensor2<double, 3, 3>
      t_dev_elastic_strain; ///< deviatoric part of the elastic trial t_strain \f$
                           ///< \boldsymbol{\varepsilon}^{e}_{\rm{dev}} =
                           ///< (\mathbb{I}^{S} -
                           ///< \frac{1}{3}\mathbf{I}\otimes\mathbf{I}) :
                           ///< \boldsymbol{\varepsilon}^{e}\f$


 t_dev_elastic_strain(i, j) = t_elastic_strain(i, j);
 t_dev_elastic_strain(0, 0) -= trace_elastic_strain_3;
 t_dev_elastic_strain(1, 1) -= trace_elastic_strain_3;
 t_dev_elastic_strain(2, 2) -= trace_elastic_strain_3;

      // calculate the deviatoric part of the trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_dev_trial_stress; ///<  deviatoric part of the trial t_stress \f$
                            ///<  \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}} =
                            ///<  \left(\frac{1}{2}(\mathbb{I} +
                            ///<  \mathbb{I}_{T})-\frac{1}{3}\mathbf{I} \otimes
                            ///<  \mathbf{I} \right) :
                            ///<  \boldsymbol{\sigma}^{\rm{trial}} = 2 \mu
                            ///<  \boldsymbol{\varepsilon}_{\rm{dev}}^{e}  \f$

      t_dev_trial_stress(i, j) = 2. * commonData.Mu * t_dev_elastic_strain(i, j);


      // calculate the reduced deviatoric trial t_stress

      FTensor::Tensor2<double, 3, 3>
          t_red_dev_stress; ///< evaluate the reduced t_stress for kinematic
                           ///< hardening \f$ \boldsymbol{\eta}_{\rm{trial}} =
                           ///< \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}} -
                           ///< \boldsymbol{\beta}  \f$

      t_red_dev_stress(i, j) = t_dev_trial_stress(i, j) - t_back_stress(i, j);


      // compute the Von Mises Criterion

      double sum_entries; ///< Compute the square of the Von Mises criterion for
                          ///< trial \f$ \left(q^{\rm{trial}}\right)^{2} =
                          ///< \frac{3}{2}||\boldsymbol{\eta}^{\rm{trial}}||^{2}
                          ///< \f$

      sum_entries = (3. / 2.) * t_red_dev_stress(i, j) * t_red_dev_stress(i, j);


      // calculate the norm of the deviatoric t_stress tensor

      double
          norm_red_dev_stress; ///< Compute the norm of the deviatoric reduced
                               ///< t_stress tensor \f$
                               ///< ||\boldsymbol{\eta}^{\rm{trial}|| \f$

      norm_red_dev_stress = sqrt(t_red_dev_stress(i, j) * t_red_dev_stress(i, j));


      // calculate the yield function
      commonData.yieldCondition =
          sqrt(sum_entries) -
          (commonData.yieldStress + commonData.hardMod * hp);

      kp_prev = kp;

      if (commonData.yieldCondition > 0) { // If the yield condition is
                                            // positive, return to the yield
                                            // surface
        double g_Value; ///< \f$\frac{\rm{d}\Phi}{\rm{d}\Delta\gamma}\f$
        commonData.cOunter = 0;

        // calculate the flow vector

        FTensor::Tensor2<double, 3, 3>
            t_flow_vector; ///< Calculate the flow vector \f$ \mathbf{N} =
                         ///< \frac{\boldsymbol{\eta}}{||\boldsymbol{\eta}||}
                         ///< \f$

        t_flow_vector(i, j) = t_red_dev_stress(i, j) / norm_red_dev_stress;


        // set plastic multiplier to 0 each time
        commonData.plasticMultiplier = 0.0;

        // Newton-Raphson loop to calculate the plastic multiplier
        do {
          g_Value =
              -3. * commonData.Mu - commonData.hardMod - commonData.kinMod;

          // update the plastic multiplier
          commonData.plasticMultiplier +=
              -commonData.yieldCondition / g_Value;

          commonData.cOunter++;

        } while (fabs(sqrt(sum_entries) -
                      3. * commonData.Mu * commonData.plasticMultiplier -
                      commonData.yieldStress -
                      commonData.kinMod * commonData.plasticMultiplier -
                      (hp + commonData.plasticMultiplier) *
                          commonData.hardMod) > commonData.tOlerance &&
                 commonData.cOunter != 2000);

        // update the hardening parameter
        hp += commonData.plasticMultiplier;

        // Update kinematic hardening parameter
        kp = commonData.kinMod * hp;

        // Update the back t_stress
        t_back_stress(i, j) += sqrt(2. / 3.) * (kp - kp_prev) * t_flow_vector(i, j);

        // Update the plastic strains
        t_plastic_str(i, j) +=
            commonData.plasticMultiplier * sqrt(3. / 2.) * t_flow_vector(i, j);

        // Update the deviatoric part of the t_stress tensor
        FTensor::Tensor2<double, 3, 3>
            t_dev_stress; ///< Update the deviatoric t_stress \f$
                         ///< \boldsymbol{\sigma}_{\rm{dev}} =
                         ///< \boldsymbol{\sigma}_{\rm{dev}}^{\rm{trial}} - 2
                         ///< \mu \Delta \gamma \sqrt{\frac{3}{2}} \mathbf{N}
                         ///< \f$

        t_dev_stress(i, j) = t_dev_trial_stress(i, j) -
                            2. * commonData.Mu * commonData.plasticMultiplier *
                                sqrt(3. / 2.) * t_flow_vector(i, j);


        // Update the elastic t_strain tensor
        t_elastic_strain(i, j) =
            (1. / (2. * commonData.Mu)) * t_dev_stress(i, j) + t_vol_strain(i, j);

        // Update the total t_stress
        t_stress(i, j) = t_dev_stress(i, j) + t_vol_pressure(i, j);

      }

      else { // If the yield function is negative calculate the t_stress as if it
             // is an elastic step

        // Calculate the elastoplastic consistent tangent matrix

        FTensor::Index<'i', 3> i;
        FTensor::Index<'j', 3> j;
        FTensor::Index<'k', 3> k;
        FTensor::Index<'l', 3> l;

        t_stress(i, j) = commonData.tD(i, j, k, l) * t_elastic_strain(k, l);
      }
      ++t_stress;
      ++t_grad;
      ++hp;
      ++t_back_stress;
      ++kp;
      ++kp_prev;
      ++t_plastic_str;
    }

    MoFEMFunctionReturn(0);
  }
};


#endif //__PLASTICITYRETURN_HPP__