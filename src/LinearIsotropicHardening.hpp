/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __ISOTROPICHARDENING_HPP__
#define __ISOTROPICHARDENING_HPP__

/**
 * @brief Implement the scale factor for Newton-Raphson method
 * 
 * The same scale factor is used throughout the analysis. 
 * The code can be improved by implementing a smarter data structure.
 * The scale factor can be changed with the "-stepsize" command line.
 * 
 */


struct LoadScale : public MethodForForceScaling {

  static double lambda;

  MoFEMErrorCode scaleNf(const FEMethod *fe, VectorDouble &nf) {
    MoFEMFunctionBegin;
    nf *= lambda;
    MoFEMFunctionReturn(0);
  }
};

double LoadScale::lambda = 1;


/**
 * @brief stores some optional data 
 * 
 * Poisson's ratio
 * Young's modulus
 */
struct BlockOptionData {
  int oRder;
  double yOung;
  double pOisson;
  double initTemp;
  BlockOptionData() : oRder(-1), yOung(-1), pOisson(-2), initTemp(0) {}
};
/**
 * @brief assemble the stiffness matrix
 *
 */
struct OpK : public VolumeElementForcesAndSourcesCore::UserDataOperator {

  // Finite element stiffness sub-matrix K_ij
  MatrixDouble K;

  CommonData &commonData;

  OpK(CommonData &common_data, bool symm = true)
      : VolumeElementForcesAndSourcesCore::UserDataOperator("U", "U", OPROWCOL,
                                                            symm),
                                                            commonData(common_data) {}

  /**
   * \brief Do calculations for give operator
   * @param  row_side row side number (local number) of entity on element
   * @param  col_side column side number (local number) of entity on element
   * @param  row_type type of row entity MBVERTEX, MBEDGE, MBTRI or MBTET
   * @param  col_type type of column entity MBVERTEX, MBEDGE, MBTRI or MBTET
   * @param  row_data data for row
   * @param  col_data data for column
   * @return          error code
   */
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type,
                        DataForcesAndSourcesCore::EntData &row_data,
                        DataForcesAndSourcesCore::EntData &col_data) {

    MoFEMFunctionBegin;

    // get number of dofs on row
    nbRows = row_data.getIndices().size();
    // if no dofs on row, exit that work, nothing to do here
    if (!nbRows)
      MoFEMFunctionReturnHot(0);

    // get number of dofs on column
    nbCols = col_data.getIndices().size();
    // if no dofs on columns, exit nothing to do here
    if (!nbCols)
      MoFEMFunctionReturnHot(0);

    // K_ij matrix will have 3 times the number of degrees of freedom of the
    // i-th entity set (nbRows)
    // and 3 times the number of degrees of freedom of the j-th entity set
    // (nbCols)
    K.resize(nbRows, nbCols, false);
    K.clear();

    // get number of integration points
    nbIntegrationPts = getGaussPts().size2();
    // check if entity block is on matrix diagonal
    if (row_side == col_side && row_type == col_type) {
      isDiag = true;
    } else {
      isDiag = false;
    }

    // integrate local matrix for entity block
    CHKERR iNtegrate(row_data, col_data);

    // assemble local matrix
    CHKERR aSsemble(row_data, col_data);

    MoFEMFunctionReturn(0);
  }

protected:
  int nbRows;           ///< number of dofs on rows
  int nbCols;           ///< number if dof on column
  int nbIntegrationPts; ///< number of integration points
  bool isDiag;          ///< true if this block is on diagonal

  
  MoFEMErrorCode
  iNtegrate(DataForcesAndSourcesCore::EntData &row_data,
            DataForcesAndSourcesCore::EntData &col_data) {
    MoFEMFunctionBegin;

    // get sub-block (3x3) of local stiffens matrix, here represented by second
    // order tensor
    auto get_tensor2 = [](MatrixDouble &m, const int r, const int c) {
      return FTensor::Tensor2<FTensor::PackPtr<double *, 3>, 3, 3>(
          &m(r + 0, c + 0), &m(r + 0, c + 1), &m(r + 0, c + 2),
          &m(r + 1, c + 0), &m(r + 1, c + 1), &m(r + 1, c + 2),
          &m(r + 2, c + 0), &m(r + 2, c + 1), &m(r + 2, c + 2));
    };

    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    FTensor::Index<'l', 3> l;

    int nb_base_functions     = row_data.getN().size2();
    MatrixDouble &Mat_Tangent = commonData.consistentMatrix;

    FTensor::Tensor4<double *, 3, 3, 3, 3> MT_1(TENSOR4_MAT_PTR(Mat_Tangent));
    // get element volume
    double vol = getVolume();

    // get intergrayion weights
    auto t_w = getFTensor0IntegrationWeight();

    // get derivatives of base functions on rows
    auto t_row_diff_base = row_data.getFTensor1DiffN<3>();
    // iterate over integration points
    for (int gg = 0; gg != nbIntegrationPts; ++gg) {

      // calculate scalar weight times element volume
      const double a = t_w * vol;

      // iterate over row base functions
      int rr = 0;
      for (; rr != nbRows / 3; ++rr) {

        // get sub matrix for the row
        auto t_m = get_tensor2(K, 3 * rr, 0);

        // get derivatives of base functions for columns
        auto t_col_diff_base = col_data.getFTensor1DiffN<3>(gg, 0);

        // iterate column base functions
        for (int cc = 0; cc != nbCols / 3;++cc) {

          // integrate block local stiffens matrix
          t_m(i, k) += a * (MT_1(i, j, k, l) *
                            (t_row_diff_base(j) * t_col_diff_base(l)));

          // move to next column base function
          ++t_col_diff_base;

          // move to next block of local stiffens matrix
          ++t_m;
        }
   
        // move to next row base function
        ++t_row_diff_base;
      }
      for (; rr != nb_base_functions; rr++) {
        ++t_row_diff_base;
      }
      // move to next integration weight
      ++t_w;
      ++MT_1;
    }

    MoFEMFunctionReturn(0);
  }

 
  MoFEMErrorCode aSsemble(DataForcesAndSourcesCore::EntData &row_data,
                                  DataForcesAndSourcesCore::EntData &col_data) {
    MoFEMFunctionBegin;
    // get pointer to first global index on row
    const int *row_indices = &*row_data.getIndices().data().begin();
    // get pointer to first global index on column
    const int *col_indices = &*col_data.getIndices().data().begin();
    // Mat B = getFEMethod()->ksp_B != PETSC_NULL ? getFEMethod()->ksp_B
    //                                            : getFEMethod()->snes_B;
    // assemble local matrix
    CHKERR MatSetValues(getFEMethod()->snes_B, nbRows, row_indices, nbCols, col_indices,
                        &*K.data().begin(), ADD_VALUES);

    if (!isDiag && sYmm) {
      // if not diagonal term and since global matrix is symmetric assemble
      // transpose term.
      K = trans(K);
      CHKERR MatSetValues(getFEMethod()->snes_B, nbCols, col_indices, nbRows, row_indices,
                          &*K.data().begin(), ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }
};


struct OpAssembleRhs
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  CommonData &commonData;
  VectorDouble nF;

  OpAssembleRhs(CommonData &common_data)
      : VolumeElementForcesAndSourcesCore::UserDataOperator(
            "U", UserDataOperator::OPROW),
        commonData(common_data) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;
    

      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0)
        MoFEMFunctionReturnHot(0);
      int nb_gauss_pts      = data.getN().size1();
      int nb_base_functions = data.getN().size2();
      nF.resize(nb_dofs, false);
      nF.clear();
      auto diff_base_functions = data.getFTensor1DiffN<3>();
      auto stress              = getFTensor2FromMat<3, 3>(*commonData.stressPtr);

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;

      for (int gg = 0; gg < nb_gauss_pts; gg++) {

        double val = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        int bb = 0;
        for (; bb != nb_dofs / 3; bb++) {
          double *ptr = &nF[3 * bb];
          FTensor::Tensor1<double *, 3> t1(ptr, &ptr[1], &ptr[2]);
          t1(i) += val * stress(i, j) * diff_base_functions(j);
          ++diff_base_functions;
        }

        // Could be that we use more base functions than needed to approx.
        // disp. field.
        for (; bb != nb_base_functions; bb++) {
          ++diff_base_functions;
        }
        ++stress;
      }


      CHKERR VecSetValues(getFEMethod()->snes_f, nb_dofs, &data.getIndices()[0],
                          &nF[0], ADD_VALUES);


    MoFEMFunctionReturn(0);
  }
};



#endif //__ISOTROPICHARDENING_HPP__
