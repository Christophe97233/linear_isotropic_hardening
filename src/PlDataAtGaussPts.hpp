
#ifndef __PLASTICITYCOMMONDATA_HPP__
#define __PLASTICITYCOMMONDATA_HPP__


struct CommonData {
 

   int cOunter = 0;
   int reactionsSidesetId;
   int maxDiv;
   double adaptPowCoef;
   double stepRed;
   double hardMod; ///<  Isotropic Hardening modulus \f$ H^{i} \f$
   double kinMod;  ///< Kinematic Hardening modulus \f$ H^{k} \f$
   double kAppa;   ///< isostatic elasticity modulus \f$ \kappa =
                   ///< \frac{E\nu}{(1-2\nu)(1 + \nu)} + \frac{2}{3}\mu \f$
   double yOung;   ///< Young's modulus \f$ E \f$
   double pOisson; ///< Poisson's ratio \f$ \nu \f$
   double Mu;      ///< shear modulus \f$ \mu = \frac{E}{2(1+\nu)} \f$
   double cOefficient;
   double
       yieldStress; ///< Prescribed value of the yield stress \f$ \sigma_{Y} \f$
   double yieldCondition;    ///< Yield criterion for Von Mises plasticity
   double plasticMultiplier; ///< Plastic multiplier \f$ \dot{\gamma} \f$
   double tOlerance;
   double
       lAmbda; ///< lame parameter \f$ \lambda = \frac{E \nu}{(1+\nu)(1-2\nu)}
               ///< \f$

   FTensor::Tensor4<double, 3, 3, 3, 3>
       tD; ///< Elastic stiffness tensor (4th rank
           ///< tensor with minor and major
           ///< symmetry) \f$ \mathbb{D} =
           ///< \mu(\mathbb{I} + \bar{\mathbb{I}}) -
           ///< \lambda(\mathbf{I} \otimes
           ///< \mathbf{I}) \f$

   boost::shared_ptr<MatrixDouble> dispPtr;

    //boost::shared_ptr<VectorDouble> dispPtr;

   //double * dispPtr;

   boost::shared_ptr<MatrixDouble> gradDispPtr; ///< Total strain tensor
                                                ///<  \f$
                                                ///< \boldsymbol{\varepsilon}
                                                ///< \f$

   MatrixDouble consistentMatrix; ///< \f$ \mathbb{C} = \mathbb{D}
                                  ///< - \frac{\mathbb{D} :
                                  ///< \mathbf{n} \otimes
                                  ///< \mathbb{D} :
                                  ///< \mathbf{n}}{\mathbf{n} :
                                  ///< \mathbb{D} : \mathbf{n} +
                                  ///< \mathbf{K}^{2}} \f$

   boost::shared_ptr<MatrixDouble> stressPtr; ///< Stress tensor \f$
                                              ///< \boldsymbol{\sigma} \f$

   boost::shared_ptr<MatrixDouble>
       plasticStrainPtr; ///< Plastic Strain \f$
                         ///< \boldsymbol{\varepsilon}^{p} =
                         ///< \boldsymbol{\varepsilon} -
                         ///< \boldsymbol{\varepsilon}^{e} \f$

   boost::shared_ptr<VectorDouble> hardParamPtr; ///< Vector containing the
                                                 ///< isotrtopic hardening
                                                 ///< parameter at each Gauss
                                                 ///< point

   boost::shared_ptr<VectorDouble> kinParamPtr; ///< Vector containing the
                                                ///< kinematic hardening
                                                ///< parameter at each Gauss
                                                ///< point

   boost::shared_ptr<VectorDouble>
       kinParamPrevPtr; ///< container that keeps the
                        ///< previous value of the
                        ///< kinetic hardening
                        ///< parameter

   boost::shared_ptr<MatrixDouble>
       backStressPtr; ///< Back stress tensor \f$ \boldsymbol{\beta} \f$

   PetscBool calcReactions;

   PetscInt testNb;

   PetscBool isAtomTest;

   CommonData(MoFEM::Interface &m_field) : mField(m_field) {
     // default base parameters

     kinMod            = 0.;
     hardMod           = 0.01;
     plasticMultiplier = 0.0;
     yieldStress       = 0.02;
     pOisson           = 0.;
     yOung             = 50000;
     tOlerance         = 1e-9;
     maxDiv            = 100;
     stepRed           = 0.2;
     adaptPowCoef      = 1. / 4.;

     gradDispPtr      = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
     stressPtr        = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
     plasticStrainPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
     hardParamPtr     = boost::shared_ptr<VectorDouble>(new VectorDouble());
     kinParamPtr      = boost::shared_ptr<VectorDouble>(new VectorDouble());
     kinParamPrevPtr  = boost::shared_ptr<VectorDouble>(new VectorDouble());
     backStressPtr    = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
     dispPtr          = boost::shared_ptr<MatrixDouble>(new MatrixDouble());

     ierr = createTags(thPlasticStrain, "PLASTIC_STRAIN", 9);
     ierr = createTags(thHardParam, "HARDENING_PARAMETER", 1);
     ierr = createTags(thBackStress, "BACK_STRESS", 9);
     ierr = createTags(thKinParam, "KIN_HARDENING_PARAMETER", 1);

     CHKERRABORT(PETSC_COMM_WORLD, ierr);
  }

  MoFEMErrorCode getTags(const EntityHandle fe_ent,
                               const int nb_gauss_pts,
                               boost::shared_ptr<MatrixDouble> tag_ptr,
                               Tag tag_ref) {
    MoFEMFunctionBegin;
    double *tag_data;
    int tag_size;
    CHKERR mField.get_moab().tag_get_by_ptr(tag_ref, &fe_ent, 1,
                                            (const void **)&tag_data,
                                            &tag_size);

    if (tag_size == 1) {
      tag_ptr->resize(9, nb_gauss_pts , false);
      tag_ptr->clear();
      void const *tag_data[] = {&*tag_ptr->data().begin()};
       const int tag_size = tag_ptr->data().size();
      CHKERR mField.get_moab().tag_set_by_ptr(tag_ref, &fe_ent, 1,
                                              tag_data, &tag_size);
    } else if (tag_size != nb_gauss_pts * 9) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Wrong size of the tag, data inconsistency");
    } else {
      MatrixAdaptor tag_vec = MatrixAdaptor(
          9, nb_gauss_pts,
          ublas::shallow_array_adaptor<double>(tag_size, tag_data));

      *tag_ptr = tag_vec;
    }

    
      MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode getTags(const EntityHandle fe_ent,
                               const int nb_gauss_pts,
                               boost::shared_ptr<VectorDouble> tag_ptr,
                               Tag tag_ref) {
    MoFEMFunctionBegin;
    double *tag_data;
    int tag_size;
    CHKERR mField.get_moab().tag_get_by_ptr(
        tag_ref, &fe_ent, 1, (const void **)&tag_data, &tag_size);

    if (tag_size == 1) {
      tag_ptr->resize(nb_gauss_pts);
      tag_ptr->clear();
      void const *tag_data[] = {&*tag_ptr->begin()};
      const int tag_size = tag_ptr->size();
      CHKERR mField.get_moab().tag_set_by_ptr(tag_ref, &fe_ent, 1,
                                              tag_data, &tag_size);
    } else if (tag_size != nb_gauss_pts) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Wrong size of the tag, data inconsistency");
    } else {
      VectorAdaptor tag_vec = VectorAdaptor(
          tag_size, ublas::shallow_array_adaptor<double>(tag_size, tag_data));
      *tag_ptr = tag_vec;
    }
    MoFEMFunctionReturn(0);
  }
 template<class T>
  MoFEMErrorCode setTags(const EntityHandle fe_ent, const int nb_gauss_pts,
                         T tag_ptr, Tag tag_ref){
    MoFEMFunctionBegin;
    void const *tag_data[] = {&*tag_ptr->data().begin()};
    const int tag_size = tag_ptr->data().size();
    CHKERR mField.get_moab().tag_set_by_ptr(tag_ref, &fe_ent, 1,
                                            tag_data, &tag_size);
    MoFEMFunctionReturn(0);
  }

  /**
   * @brief Get the Parameters object
   * 
   * @return MoFEMErrorCode
   * 
   * create the function to pass some command lines to the main file. 
   */
  MoFEMErrorCode getParameters() {
    MoFEMFunctionBegin;

    reactionsSidesetId = 0;
    calcReactions = PETSC_TRUE;
    testNb = 1;
    isAtomTest = PETSC_FALSE;

    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "Material parameters",
                             "none");

    CHKERR PetscOptionsScalar("-young", "Young modulus", "", yOung, &yOung,
                              PETSC_NULL);

    CHKERR PetscOptionsScalar("-poisson", "Poisson ratio", "", pOisson,
                              &pOisson, PETSC_NULL);

    CHKERR PetscOptionsScalar("-tolerance", "tolerance", "", tOlerance,
                              &tOlerance, PETSC_NULL);

    CHKERR PetscOptionsScalar("-yield", "Yield stress", "", yieldStress ,
                              &yieldStress , PETSC_NULL);

    CHKERR PetscOptionsScalar("-HardMod", "Hardening Modulus", "", hardMod,
                              &hardMod, PETSC_NULL);

    CHKERR PetscOptionsScalar("-KinMod", "Kinematic Hardening Modulus", "",
                              kinMod, &kinMod, PETSC_NULL);

    CHKERR PetscOptionsScalar("-step_red", "step reductcion while diverge", "",
                              stepRed, &stepRed, PETSC_NULL);

    CHKERR PetscOptionsScalar("-adapt_coef",
                              "code will be smarter depending on the value", "",
                              adaptPowCoef, &adaptPowCoef, PETSC_NULL);

    CHKERR PetscOptionsInt(
        "-calc_reactions", "calculate reactions for blocksets", "",
        reactionsSidesetId, &reactionsSidesetId, &calcReactions);

    CHKERR PetscOptionsInt("-is_atom_test", "Give number of the atom test", "",
                           testNb, &testNb, &isAtomTest);

    CHKERR PetscOptionsInt("-steps_max_div", "number of steps", "", maxDiv,
                           &maxDiv, PETSC_NULL);


    ierr           = PetscOptionsEnd();
    CHKERRQ(ierr);


    cOefficient = yOung / ((1 + pOisson) * (1 - 2 * pOisson));
    Mu = yOung / (2 * (1 + pOisson));
    kAppa = cOefficient * pOisson + (2. / 3.) * Mu;
     lAmbda      = cOefficient * pOisson;
     FTensor::Index<'i', 3> i;
     FTensor::Index<'j', 3> j;
     FTensor::Index<'k', 3> k;
     FTensor::Index<'l', 3> l;
     tD(i, j, k, l) = 0.;
  
    tD(0, 0, 0, 0) = lAmbda + 2 * Mu;
    tD(1, 1, 1, 1) = lAmbda + 2 * Mu;
    tD(2, 2, 2, 2) = lAmbda + 2 * Mu;
    tD(0, 0, 1, 1) = lAmbda;
    tD(0, 0, 2, 2) = lAmbda;
    tD(1, 1, 2, 2) = lAmbda;
    tD(2, 2, 1, 1) = lAmbda;
    tD(1, 1, 2, 2) = lAmbda;
    tD(2, 2, 0, 0) = lAmbda;
    tD(1, 1, 0, 0) = lAmbda;
    tD(0, 1, 0, 1) = Mu;
    tD(0, 2, 0, 2) = Mu;
    tD(1, 2, 1, 2) = Mu;
    tD(0, 1, 1, 0) = Mu;
    tD(0, 2, 2, 0) = Mu;
    tD(1, 2, 2, 1) = Mu;
    tD(2, 0, 0, 2) = Mu;
    tD(1, 0, 0, 1) = Mu;
    tD(2, 1, 1, 2) = Mu;
    tD(2, 0, 2, 0) = Mu;
    tD(1, 0, 1, 0) = Mu;
    tD(2, 1, 2, 1) = Mu;

// ierr           = PetscOptionsEnd();
//     CHKERRQ(ierr);
    MoFEMFunctionReturn(0);
  }

  Tag thPlasticStrain;
  Tag thHardParam;
  Tag thBackStress;
  Tag thKinParam;

  // MoFEMErrorCode
  // calcReactionForces(VectorDouble &reactions, DM &dm,
  //                    boost::shared_ptr<VolumeElementForcesAndSourcesCore> feRhs,
  //                    int &reactionsSidesetId, MoFEM::Interface &mField);

private:
  MoFEM::Interface &mField;

  MoFEMErrorCode createTags(Tag &tag_ref,
                            const string tag_string, const int &dim){
  
    MoFEMFunctionBegin;
    std::vector<double> def_val(dim, 0);
    CHKERR mField.get_moab().tag_get_handle(
        tag_string.c_str(), 1, MB_TYPE_DOUBLE, tag_ref,
        MB_TAG_CREAT | MB_TAG_VARLEN | MB_TAG_SPARSE, &def_val[0]);
    MoFEMFunctionReturn(0);
  }

  public:
    MoFEMErrorCode
  calcReactionForces(VectorDouble &reactions, DM &dm,
                     boost::shared_ptr<VolumeElementForcesAndSourcesCore> feRhs,
                     int &reactionsSidesetId);

    MoFEMErrorCode atomTests(const int step_number,
                             const VectorDouble &reactions);

};

#endif //__PLASTICITYCOMMONDATA_HPP__