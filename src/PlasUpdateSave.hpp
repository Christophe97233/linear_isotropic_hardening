#ifndef __UPDATESAVE_HPP__
#define __UPDATESAVE_HPP__

/**
 * @brief Update the tags (plastic strain and hardening parameter) if and only if the convergence criterion is met
 * 
 */
struct OpUpdate //Update the plastic strain when the convergence criterion is met 
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface &mField;
  CommonData &commonData;

  OpUpdate(MoFEM::Interface &m_field, string field_name,
           CommonData &common_data)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      MoFEMFunctionReturnHot(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      MoFEMFunctionReturnHot(0);

    //get the number of Gauss points
    int nb_gauss_pts = data.getN().size1();
  
    EntityHandle fe_ent = getFEEntityHandle();

    //set the value of the plastic strian to the tag (if converged)
    CHKERR commonData.setTags<MatrixDouble *>(fe_ent, nb_gauss_pts,
                                              commonData.plasticStrainPtr.get(),
                                              commonData.thPlasticStrain);

    CHKERR commonData.setTags<MatrixDouble *>(fe_ent, nb_gauss_pts,
                                              commonData.backStressPtr.get(),
                                              commonData.thBackStress);

    CHKERR commonData.setTags<VectorDouble *>(fe_ent, nb_gauss_pts,
                                              commonData.hardParamPtr.get(),
                                              commonData.thHardParam);

    CHKERR commonData.setTags<VectorDouble *>(fe_ent, nb_gauss_pts,
                                              commonData.kinParamPtr.get(),
                                              commonData.thKinParam);

    MoFEMFunctionReturn(0);
                        }
};

/**
 * @brief Postprocess the plastic strains and the hardening parameter at the Gauss points
 * 
 */
struct OpSavePlasticStrain
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  moab::Interface &moabOut;
  MoFEM::Interface &mField;
  CommonData &commonData;

  OpSavePlasticStrain(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data, moab::Interface &moab_out)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data),moabOut(moab_out) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      MoFEMFunctionReturnHot(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      MoFEMFunctionReturnHot(0);
    int nb_gauss_pts    = data.getN().size1();


   double def_vals[9];
   bzero(def_vals,9*sizeof(double));

   Tag th_strain;
   CHKERR moabOut.tag_get_handle("PLASTIC_STRAIN0", 9, MB_TYPE_DOUBLE,
                                 th_strain, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);
   Tag th_stress;
   CHKERR moabOut.tag_get_handle("STRESS", 9, MB_TYPE_DOUBLE,
                                 th_stress, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);

    Tag th_back;
   CHKERR moabOut.tag_get_handle("BACK_STRESS0", 9, MB_TYPE_DOUBLE,
                                 th_back, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);
   Tag th_disp;
   CHKERR moabOut.tag_get_handle("DISPLACEMENT", 3, MB_TYPE_DOUBLE,
                                 th_disp, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);

   Tag th_hard;
  CHKERR moabOut.tag_get_handle("HARDENING0", 1, MB_TYPE_DOUBLE,
                                 th_hard, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);

  Tag th_kin;
  CHKERR moabOut.tag_get_handle("KIN_HARDENING0", 1, MB_TYPE_DOUBLE, th_kin,
                                MB_TAG_CREAT | MB_TAG_SPARSE, def_vals);

  double coords[3];
  EntityHandle new_vertex = getFEEntityHandle();
  CHKERR commonData.getTags(new_vertex, nb_gauss_pts,
                            commonData.plasticStrainPtr,
                            commonData.thPlasticStrain);

  CHKERR commonData.getTags(new_vertex, nb_gauss_pts, commonData.hardParamPtr,
                            commonData.thHardParam);

  CHKERR commonData.getTags(new_vertex, nb_gauss_pts, commonData.kinParamPtr,
                            commonData.thKinParam);

  CHKERR commonData.getTags(new_vertex, nb_gauss_pts, commonData.backStressPtr,
                            commonData.thBackStress);

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {
    for (int dd = 0; dd != 3; dd++) {
      coords[dd] = getCoordsAtGaussPts()(gg, dd);
    }

    // plastic strain
    auto t_plastic_str = getFTensor2FromMat<3, 3>(*commonData.plasticStrainPtr);

    // t_stress
    auto t_stress = getFTensor2FromMat<3, 3>(*commonData.stressPtr);

    // Hardening Parameter
    auto hp = getFTensor0FromVec(*commonData.hardParamPtr);

    // Kinematic hardening parameter
    auto kp = getFTensor0FromVec(*commonData.kinParamPtr);

    // Back Stress
    auto t_back_stress = getFTensor2FromMat<3, 3>(*commonData.backStressPtr);

    VectorDouble &data_disp = data.getFieldData();
    CHKERR moabOut.create_vertex(&coords[0], new_vertex);
    CHKERR moabOut.tag_set_data(th_strain, &new_vertex, 1, &t_plastic_str(0, 0));
    CHKERR moabOut.tag_set_data(th_stress, &new_vertex, 1, &t_stress(0, 0));
    CHKERR moabOut.tag_set_data(th_back, &new_vertex, 1, &t_back_stress(0, 0));
    CHKERR moabOut.tag_set_data(th_disp, &new_vertex, 1, &data_disp[0]);
    CHKERR moabOut.tag_set_data(th_hard, &new_vertex, 1, &hp);
    CHKERR moabOut.tag_set_data(th_kin, &new_vertex, 1, &kp);

    ++t_stress;
    ++hp;
    ++t_plastic_str;
    ++t_back_stress;
    ++kp;
   }
   MoFEMFunctionReturn(0);
  }
};


        struct OpPrint : public VolumeElementForcesAndSourcesCore::UserDataOperator {
          using SetPtsData = FieldEvaluatorInterface::SetPtsData;
          std::array<double, 3> point;
          std::string str;
          CommonData &commonData;
          boost::shared_ptr<SetPtsData> dataFieldEval;
          MoFEM::Interface &mfield;
          int &it;
          using VolEle = VolumeElementForcesAndSourcesCore;
          OpPrint(std::array<double, 3> &point, std::string &str,
                  CommonData &common_data, MoFEM::Interface &m_field, int &It)
              : VolumeElementForcesAndSourcesCore::UserDataOperator(
                    "U",
                    VolumeElementForcesAndSourcesCore::UserDataOperator::OPROW),
                point(point), str(str), commonData(common_data),
                dataFieldEval(m_field.getInterface<FieldEvaluatorInterface>()
                                  ->getData<VolEle>()),
                mfield(m_field), it(It) {}

          MoFEMErrorCode doWork(int side, EntityType type,
                                DataForcesAndSourcesCore::EntData &data) {
            MoFEMFunctionBegin;
            dataFieldEval->setEvalPoints(point.data(), point.size() / 3);

                if (type != MBVERTEX)
                  MoFEMFunctionReturnHot(0);

                int nb_dofs = data.getFieldData().size();
                if (nb_dofs == 0)
                  MoFEMFunctionReturnHot(0);
                int nb_gauss_pts = data.getN().size1();
                commonData.dispPtr.get()->resize(3, nb_gauss_pts, false);
               // double *disp_ptr = commonData.dispPtr;
               auto disp        = getFTensor1FromMat<3>(*commonData.dispPtr);
                // auto disp        = getFTensor1FromMat(*commonData.dispPtr);

                auto add = [&]() {
                  std::ostringstream s;
                  s << str << " elem " << getFEEntityHandle() << " ";
                  return s.str();
                };

                auto print_tensor = [](auto &t) {
                  std::ostringstream s;
                  s << t;
                  return s.str();
                };
                // double coords[3];
                VectorDouble coords = getCoords();
             
                if (it < 1){
                for (int gg = 0; gg != sizeof(coords); gg++) {
            
                  
                  if (fabs(coords[gg] - point[0]) < 1e-12 && fabs(coords[gg+1] - point[1]) < 1e-12 && fabs(coords[gg+2] - point[2]) < 1e-12  ){
           
                std::cout << print_tensor(disp)<< endl;
                ++it;
                  }

                ++disp;
                }
                }

            MoFEMFunctionReturn(0);
                                }
        };

// ;

#endif //__UPDATESAVE_HPP__