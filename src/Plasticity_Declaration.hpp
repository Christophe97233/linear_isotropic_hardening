
#ifndef __PLASTICITYDECLARATION_HPP__
#define __PLASTICITYDECLARATION_HPP__


/**
 * @brief Get the material parameters from the command line
 * 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode getParameters();

/**
 * @brief scales the load factor
 * 
 * @param fe 
 * @param nf 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode scaleNf(const FEMethod *fe, VectorDouble &nf);
/**
 * @brief Create a Tag for kinematic hardening parameter
 *
 * @param moab
 * @return MoFEMErrorCode
 */
MoFEMErrorCode createTagKinHardening(moab::Interface &moab);

/**
 * @brief Create the tag for the back stress
 * 
 * @param moab 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode createTagBackStress(moab::Interface &moab);

/**
 * @brief Get the Plastic Strain from the Tag.
 * 
 * @param fe_ent 
 * @param nb_gauss_pts number of gauss points
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode getPlasticStrain(const EntityHandle fe_ent,
                                const int nb_gauss_pts);

/**
 * @brief Get the hardening parameters at the Gauss points
 * 
 * @param fe_ent 
 * @param nb_gauss_pts number of gauss points
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode getHardParam(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
 * @brief Get the back stress from the tag.
 * 
 * @param fe_ent 
 * @param nb_gauss_pts 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode getBackStress(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
 * @brief Create the tag for the plastic strains
 * 
 * @param moab 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode createTagPlastic(moab::Interface &moab);

/**
 * @brief create the tag for Hardening parameters
 * 
 * @param moab 
 * @return MoFEMErrorCode 
 * 
 */
MoFEMErrorCode createTagIsHardening(moab::Interface &moab);

/**
 * @brief Create the tag to save back stress.
 * 
 * @param moab 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode createTagBackStress(moab::Interface &moab);

/**
 * @brief Set the Plastic Strain on the tag.
 * 
 * @param fe_ent 
 * @param nb_gauss_pts number of gauss points
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode setPlasticStrain(const EntityHandle fe_ent,
                                const int nb_gauss_pts);

/**
 * @brief Set the Hardening parameter on the tag.
 * 
 * @param fe_ent 
 * @param nb_gauss_pts number of gauss points
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode setHardParam(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
 * @brief Set the back stress on the Tag.
 * 
 * @param fe_ent 
 * @param nb_gauss_pts 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode setBackStress(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
 * @brief Get kinematic hardening parameter
 * 
 * @param fe_ent 
 * @param nb_gauss_pts 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode getKinParam(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
 * @brief Set the kinematic hardening parameter on the tag
 * 
 * @param fe_ent 
 * @param nb_gauss_pts 
 * @return MoFEMErrorCode 
 */
MoFEMErrorCode setKinParam(const EntityHandle fe_ent, const int nb_gauss_pts);

/**
   * \brief Integrate B^T D B operator
   * @param  row_data row data (consist base functions on row entity)
   * @param  col_data column data (consist base functions on column entity)
   * @return error code
   */
MoFEMErrorCode iNtegrate(DataForcesAndSourcesCore::EntData &row_data,
                         DataForcesAndSourcesCore::EntData &col_data);
  /**
   * \brief Assemble local entity block matrix
   * @param  row_data row data (consist base functions on row entity)
   * @param  col_data column data (consist base functions on column entity)
   * @return          error code
   */
MoFEMErrorCode aSsemble(DataForcesAndSourcesCore::EntData &row_data,
                        DataForcesAndSourcesCore::EntData &col_data);


#endif //__PLASTICITYDECLARATION_HPP__
